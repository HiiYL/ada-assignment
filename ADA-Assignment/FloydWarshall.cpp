//
//  FloydWarshall.cpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii and Ng Bing Yuan on 16/01/2016.
//  Copyright © 2016 Yong Lian Hii and Ng Bing Yuan. All rights reserved.
//

#include "FloydWarshall.hpp"
#include "Timer.hpp"
FloydWarshall::FloydWarshall(AdjacencyMatrix &matrix): adjMatrix(matrix) {
    dist = std::vector< std::vector<int> > (adjMatrix.getMatrix());
    long vertex_count = adjMatrix.getVertexCount();
    Timer timer;
    timer.start();
    for (int k = 0; k < vertex_count; k++)
    {

        for (int i = 0; i < vertex_count; i++)
        {
            for (int j = 0; j <vertex_count; j++)
            {
                if(i==j || i==k || j==k || dist[i][k] == INF || dist[k][j] == INF)
                    continue;
                if (dist[i][k] + dist[k][j] < dist[i][j]) {
                    //                    cout << k << " " << i << " " << j << endl;
                    //                    cout << i << k << " " << k<< j << " " << i << j << endl;
                    //                    cout << endl;
                    
                    dist[i][j] = dist[i][k] + dist[k][j];
                }
                //                cout << endl;
            }
        }
    }
    timer.stop();
    timeTaken = timer.getElapsedTimeInMicroSec();
};
void FloydWarshall::printDistances(int source) {
    if (source == INF) {
        std::cout << " --- Result in Adjacency Matrix --- " << std::endl;
        for (const auto &i : dist)
        {
            for (const auto &j : i)
            {
                if (j == INF)
                    printf("%7s", "INF");
                else
                    printf ("%7d", j);
            }
            std::cout << std::endl;
        }
    }else {
        std::cout << "Printing for Vertex " << source << std::endl;
        for (const auto &i : dist[source]) {
            if (i == INF)
                printf("%7s", "INF");
            else
                printf ("%7d", i);
        }
    }
    
}
