//
//  Dijkstra.cpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii and Ng Bing Yuan on 16/01/2016.
//  Copyright © 2016 Yong Lian Hii and Ng Bing Yuan. All rights reserved.
//

#include "Dijkstra.hpp"
#include "Timer.hpp"
Dijkstra::Dijkstra(const AdjacencyList &adjList, bool verbose):adjList(adjList) {
    min_distance_all_pairs = std::vector<std::vector<int>> (adjList.getVertexCount(), std::vector<int>());
        dijAllPairs(adjList.getList(), verbose);
}
void Dijkstra::printResult(int source) {
    std::cout << "Vertex\tVisited \tCost\tFrom\tShortest Path from "<< source << std::endl;
    for(int i = 0; i < min_distance_all_pairs[source].size(); i++){
        printf("%6d\t",i);
        printf("%7s \t",visited[i]?"1":"0");
        if (min_cost[i]==INF)
            printf("%4s\t","INF");
        else
            printf("%4d\t",min_cost[i]);
        printf("%4d\t",from[i]);
        shortestpath(i,from);
        std::cout << std::endl;
    }
    std::cout<<std::endl;
}
void Dijkstra::printDistances(int source) {
    if (source == INF) {
        std::cout << " --- Result in Adjacency Matrix --- " << std::endl;
        for (const auto &i : min_distance_all_pairs)
        {
            for (const auto &j : i)
            {
                if (j == INF)
                    printf("%7s", "INF");
                else
                    printf ("%7d", j);
            }
            std::cout << std::endl;
        }
    }else {
        std::cout << "Printing for Vertex " << source << std::endl;
        for (const auto &i : min_distance_all_pairs[source]) {
            if (i == INF)
                printf("%7s", "INF");
            else
                printf ("%7d", i);
        }
    }
}

void Dijkstra::dijkstra(int source, const std::vector<std::vector<Edge>> &adjacency_list, std::vector<bool> &visited,std::vector<int> &min_cost, std::vector<int> &from){
    visited[source] = true;
    //set cost for source
    min_cost[source] = 0;
    //min sorted
    std::set< std::pair<int,int> > priorityQ;
    priorityQ.insert({min_cost[source], source});
    
    // dijkstra main loop
    //NOTE: I reversed the cost and vertex for your algorithm, this is because set sorts based on first value
    //In your original algorithm, it is actually sorting based on your vertex value
    while(!priorityQ.empty()){
        int vertex = priorityQ.begin()->second;
        int cost = priorityQ.begin()->first;
        visited[vertex] = true;
        priorityQ.erase(priorityQ.begin());
        const std::vector<Edge> &next = adjacency_list[vertex];
        for (int i = 0; i < next.size();i++){
            int next_to = next[i].to;
            int next_cost = next[i].cost;
            int new_cost = cost + next_cost;
            // cout << "now at" << next_v << "cost "<< new_cost << endl;
            // if new cost < then existing cost
            // set path & replace cost with new cost
            if (new_cost < min_cost[next_to]) {
                from[next_to]=vertex;
                min_cost[next_to]=new_cost;
                priorityQ.insert(std::make_pair(new_cost,next_to));
            }
        }
    }
}
void Dijkstra::shortestpath(int current, std::vector<int> &from){
    if(from[current]!=-1)
        shortestpath(from[current],from);
    std::cout<<current<<" ";
}

void Dijkstra::dijAllPairs(const std::vector<std::vector<Edge>>&adjList, bool verbose) {
    std::vector<std::chrono::duration<long long, std::nano>> duration;
    long size = adjList.size();
    for(int source = 0; source < adjList.size(); source++){
        visited.clear();
        visited.resize(size,false);
        min_cost.clear();
        //set cost for all
        min_cost.resize(size,INF);
        from.clear();
        //init path from to -1
        from.resize(size,-1);
        Timer timer;
        timer.start();
        dijkstra(source, adjList,visited, min_cost, from);
        timer.stop();
        timeTaken += timer.getElapsedTimeInMicroSec();
        min_distance_all_pairs[source] = min_cost;
        
        if (verbose)
            printResult(source);
    }
}