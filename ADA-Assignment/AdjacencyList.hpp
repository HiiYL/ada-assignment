//
//  AdjacencyList.hpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii and Ng Bing Yuan on 16/01/2016.
//  Copyright © 2016 Yong Lian Hii and Ng Bing Yuan. All rights reserved.
//

#ifndef AdjacencyList_hpp
#define AdjacencyList_hpp

#include "AdjacencyMatrix.hpp"
class AdjacencyList {
public:
    AdjacencyList(AdjacencyMatrix matrix) {
        list = generateAdjList(matrix);
    }
    std::vector<std::vector<Edge>> getList() const {
        return list;
    }
    long getVertexCount() const {
        return list.size();
    }
private:
    std::vector<std::vector<Edge>> list;
    std::vector<std::vector<Edge>> generateAdjList(AdjacencyMatrix adjMatrix) {
        std::vector<std::vector<int> > matrix = adjMatrix.getMatrix();
        long vertex_count = adjMatrix.getVertexCount();
        std::vector<std::vector<Edge>> graphList(vertex_count, std::vector<Edge>());
        for (int i = 0; i < vertex_count; i++) {
            for (int j = 0; j < vertex_count; j++) {
                graphList[i].push_back(Edge(j, matrix[i][j]));
            }
        }
        return graphList;
    }
};

#endif /* AdjacencyList_hpp */
