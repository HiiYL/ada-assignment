//
//  Dijkstra.hpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii and Ng Bing Yuan on 16/01/2016.
//  Copyright © 2016 Yong Lian Hii and Ng Bing Yuan. All rights reserved.
//

#ifndef Dijkstra_hpp
#define Dijkstra_hpp

#include <vector>
#include <set>
#include <chrono>
#include <iostream>

#include "AdjacencyList.hpp"


typedef std::chrono::high_resolution_clock Clock;

class Dijkstra {
public:
    Dijkstra(const AdjacencyList &adjList, bool useBingAlgorithm);
    void printResult(int source);
    void printDistances(int source = INF);
    double getTimeTaken() {return timeTaken; };
    
private:
    std::vector<int> min_cost;
    std::vector<int> from;
    std::vector<bool> visited;
    const AdjacencyList& adjList;
    std::vector<std::vector<int> > min_distance_all_pairs;
    std::vector<std::vector<int> > dist;
    double timeTaken;
    
    void shortestpath(int current, std::vector<int> &from);
    void dijkstra(int source, const std::vector<std::vector<Edge>> &adjacency_list, std::vector<bool> &visited,std::vector<int> &min_cost, std::vector<int> &from);
    void dijAllPairs(const std::vector<std::vector<Edge>>&adjList, bool verbose = true);
    
};


#endif /* Dijkstra_hpp */
