//
//  AdjacencyMatrix.hpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii and Ng Bing Yuan on 16/01/2016.
//  Copyright © 2016 Yong Lian Hii and Ng Bing Yuan. All rights reserved.
//

#ifndef AdjacencyMatrix_hpp
#define AdjacencyMatrix_hpp

#include <vector>
#include <random>
#define INF 99999

struct Edge {
    Edge(int to,int cost): to(to), cost(cost) {};
    int to, cost;
};

class AdjacencyMatrix {
public:
    AdjacencyMatrix(long vertex_count, long edge_count):vertex_count(vertex_count), edge_count(edge_count) {
        matrix = generateAdjMatrix(vertex_count, edge_count);
    }
    AdjacencyMatrix(const std::vector<std::vector<int>>& matrix):matrix(matrix) {
        vertex_count = matrix.size();
    
    };
    std::vector<std::vector<int>> getMatrix() {
        return matrix;
    }
    long getVertexCount() {
        return vertex_count;
    }
private:
    long vertex_count;
    long edge_count;
    std::vector<std::vector<int>> matrix;
    
    
    std::vector<std::vector<int>> generateAdjMatrix(long vertex_count, long edge_count) {
        std::vector<std::vector<int>> graph(vertex_count, std::vector<int>(vertex_count, INF));
        
        std::uniform_int_distribution<int>  distr(0, vertex_count - 1);
        std::random_device                  rand_dev;
        std::mt19937                        generator(rand_dev());
        for (int i = 0; i < edge_count; i++) {
            int u = distr(generator);
            int v = distr(generator);
            
            int weight = rand() % 10;
            
            if (u != v && graph[v][u] == INF && graph[u][v] == INF) {
                graph[u][v] = weight;
            }else {
                i--;
            }
        }
        for (int i = 0; i < vertex_count; i++) {
            graph[i][i] = 0;
        }
        return graph;
    }
    

};

#endif /* AdjacencyMatrix_hpp */
