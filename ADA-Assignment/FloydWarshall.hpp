//
//  FloydWarshall.hpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii and Ng Bing Yuan on 16/01/2016.
//  Copyright © 2016 Yong Lian Hii and Ng Bing Yuan. All rights reserved.
//

#ifndef FloydWarshall_hpp
#define FloydWarshall_hpp
#define INF 99999
#include "AdjacencyMatrix.hpp"
#include <iostream>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;
class FloydWarshall {
public:
    FloydWarshall(AdjacencyMatrix &matrix);
    void printDistances(int source = INF);
    double getTimeTaken() {
        return timeTaken;
    };
private:
    AdjacencyMatrix &adjMatrix;
    std::vector< std::vector<int> > dist;
    double timeTaken;
};

#endif /* FloydWarshall_hpp */
