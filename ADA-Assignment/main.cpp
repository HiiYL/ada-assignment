//
//  main.cpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii and Ng Bing Yuan on 09/01/2016.
//  Copyright © 2016 Yong Lian Hii and Ng Bing Yuan. All rights reserved.
//
#define INF 99999
#include <iostream>
#include <chrono>
#include <algorithm>
#include "AdjacencyMatrix.hpp"
#include "AdjacencyList.hpp"
#include "FloydWarshall.hpp"
#include "Dijkstra.hpp"
#include "Utility.hpp"
using namespace std;

typedef std::chrono::high_resolution_clock Clock;

int main()
{
    /* Let us create the following weighted graph
         2
     (0)<--(5)    (1)
      |           /|\
    5 |            |
      |            | 1
     \|/           |
     (3)--------->(2)
           3           */
    vector<vector<int>> graph= {
        {0,   INF,  INF, 5,INF},
        {INF, 0,   INF, INF,INF},
        {INF, 1, 0,   INF,INF},
        {INF, INF, 3, 0,INF},
        {2,INF,INF,INF,0}
    };
    
    int choice;
    
    cout << " -------------------------------" << endl
         << "             GRAPH DEMO         " << endl
         << " -------------------------------" << endl;
    cout << " Choice: " << endl
         << "0 - Demo Graph" << endl
         << "1 - Set Graph " << endl
         << ">>";
    cin >> choice;
    switch (choice) {
        case 0: {
            
            AdjacencyMatrix adjMatrix = AdjacencyMatrix(graph);
            AdjacencyList adjList = AdjacencyList(adjMatrix);
            Dijkstra dijkstra = Dijkstra(adjList, true);
            FloydWarshall f_warshall = FloydWarshall(adjMatrix);
            
            cout << "Dijkstra - " << dijkstra.getTimeTaken() << " microseconds" << endl;
            cout << "Floyd Warshall - " <<  f_warshall.getTimeTaken() << " microseconds" <<  endl;
            
            dijkstra.printDistances();
            f_warshall.printDistances();
            
            
            break;
        }
            
        case 1: {
            vector<int> vertex_count_to_test;
            vector<int> edge_count_to_test;
            cout << "Enter Vertices Count ( Leave Blank for Empty )" << endl
                 << ">> ";
            cin.ignore();
            string input;
            getline(cin, input);

            if (!input.empty()) {
                vector<string> vertex_count_str = Utility::split(input, ' ');
                std::transform(vertex_count_str.begin(), vertex_count_str.end(),
                               std::back_inserter(vertex_count_to_test),
                               [](const std::string& str) { return std::stoi(str); });
            }else {
                vertex_count_to_test = { 10, 20, 50, 100 ,150, 200 };
            }
            
            float percentage;
            cout << "Enter Density of Graph ( % )" << endl
                 << ">> ";
            cin >> percentage;
            for(const auto &vertex_count : vertex_count_to_test) {
                float max = (vertex_count * (vertex_count - 1)) /2;
                edge_count_to_test.push_back(percentage/100 * max);
            }
            
            for(int i = 0 ; i < vertex_count_to_test.size(); i++) {
                int vertex_count = vertex_count_to_test[i];
                int edge_count = edge_count_to_test[i];
        
                if (edge_count > ((vertex_count-1)*(vertex_count)/2)) {
                    cout << "Max edge count is " << ((vertex_count-1)*(vertex_count)/2) << endl;
                    return -1;
                }
        
                cout << "         --- Current Running  ---  " << endl;
                cout << "   Vertex Count - " <<  vertex_count << " Edge Count - " << edge_count << endl;

                AdjacencyMatrix adjMatrix = AdjacencyMatrix(vertex_count, edge_count);
                AdjacencyList adjList = AdjacencyList(adjMatrix);
                FloydWarshall f_warshall = FloydWarshall(adjMatrix);
                Dijkstra dijkstra = Dijkstra(adjList, false);
                cout << "Dijkstra - " << dijkstra.getTimeTaken()<< " microseconds" <<  endl;
                cout << "Floyd Warshall - " <<  f_warshall.getTimeTaken()  << " microseconds" << endl;
                cout << endl << endl;
                }
            break;
        }
        default:
            break;
    }
    


    return 0;
}
