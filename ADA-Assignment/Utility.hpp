//
//  Utility.hpp
//  ADA-Assignment
//
//  Created by Yong Lian Hii on 25/01/2016.
//  Copyright © 2016 Yong Lian Hii. All rights reserved.
//

#ifndef Utility_hpp
#define Utility_hpp
#include <vector>
#include <sstream>

class Utility {
public:
    static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }
    static std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, elems);
        return elems;
    }
};



#endif /* Utility_hpp */
